// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Rebellion.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Rebellion, "Rebellion" );

DEFINE_LOG_CATEGORY(LogRebellion)
 