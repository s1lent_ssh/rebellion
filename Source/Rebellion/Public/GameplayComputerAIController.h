// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameplayComputerAIController.generated.h"

/**
 * 
 */
UCLASS()
class REBELLION_API AGameplayComputerAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	AGameplayComputerAIController(const FObjectInitializer& ObjectInitializer);

};
